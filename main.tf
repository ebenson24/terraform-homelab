terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = ">=2.9.14"
    }
  }
  required_version = ">=1.4.2"
}

resource "proxmox_vm_qemu" "testingserver" {
  name = "Test Server"
  target_node = "pve"
  iso = "local:iso/ubuntu-22.04.1-live-server-amd64.iso"
}

resource "proxmox_vm_qemu" "JenkinsServer" {
  name        = "Jenkins"
  target_node = "pve"
  iso         = "local:iso/ubuntu-22.04.1-live-server-amd64.iso"
  scsihw      = "virtio-scsi-single"
  memory      = "4096"
  sockets     = "1"
  cores       = 4
  disk {
    type    = "scsi"
    storage = "local-lvm"
    size    = "50G"
  }
  network {
    bridge = "vmbr0"
    model  = "virtio"
  }
}